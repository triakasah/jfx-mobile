angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicLoading, $http, $ionicSlideBoxDelegate, $stateParams, judul_kategori, pagination_status, $cordovaSocialSharing) {
  // Loading
  $scope.showLoading = function() {
      $ionicLoading.show({
         template: 'Memuat Berita ...'
      });
   };

   // Refresh Beranda
   $scope.refresh_beranda = function() {
       $scope.news_slidebox();
       $scope.news_terbaru();
       $scope.news_terbaru_detail();
       $scope.$broadcast('scroll.refreshComplete');
  };

   // Berita di SlideBox
  $scope.news_slidebox = function(){
    $http.get(
      'http://www.jfxjournal.com/api/posts/lastest/artikel'
    ).success(function(data){
      $scope.result_news_slidebox=data.result;
      $ionicLoading.show({
           template: 'Selesai memuat berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
            $ionicSlideBoxDelegate.update();
        }, 1000);
    }).error(function(){
      $ionicLoading.show({
           template: 'Gagal memuat berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
        }, 5000);
    });
  };

   // Berita Terbaru
  $scope.news_terbaru = function(){
    $http.get(
      'http://www.jfxjournal.com/api/posts/lastest/all'
    ).success(function(data){
      $scope.result_news_terbaru=data.result;
      $ionicLoading.show({
           template: 'Selesai memuat berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
            $ionicSlideBoxDelegate.update();
        }, 1000);
    }).error(function(){
      $ionicLoading.show({
           template: 'Gagal memuat berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
        }, 5000);
    });
  };

   // Berita Terbaru Detail
  $scope.news_terbaru_detail = function(){
    $http.get(
      'http://www.jfxjournal.com/api/posts/read/'+$stateParams.id+' '
    ).success(function(data){
      $scope.result_news_terbaru_detail=data.result;
      $ionicLoading.show({
           template: 'Selesai memuat detail berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
            $ionicSlideBoxDelegate.update();
        }, 1000);
    }).error(function(){
      $ionicLoading.show({
           template: 'Gagal memuat detail berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
        }, 5000);
    });
  };

   // Berita Populer
  $scope.news_populer = function(){
    $http.get(
      'http://www.jfxjournal.com/api/posts/popular/all'
    ).success(function(data){
      $scope.result_news_populer=data.result;
      $ionicLoading.show({
           template: 'Selesai memuat berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
            $ionicSlideBoxDelegate.update();
        }, 1000);
    }).error(function(){
      $ionicLoading.show({
           template: 'Gagal memuat berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
        }, 5000);
    });
  };

   // Berita Terbaru Detail
  $scope.news_populer_detail = function(){
    $http.get(
      'http://www.jfxjournal.com/api/posts/read/'+$stateParams.id+' '
    ).success(function(data){
      $scope.result_news_populer_detail=data.result;
      $ionicLoading.show({
           template: 'Selesai memuat detail berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
            $ionicSlideBoxDelegate.update();
        }, 1000);
    }).error(function(){
      $ionicLoading.show({
           template: 'Gagal memuat detail berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
        }, 5000);
    });
  };

   // Berita per kategori
   // Refresh Beranda
   $scope.refresh_category = function() {
       $scope.getjudul();
       $scope.news_category();
       $scope.$broadcast('scroll.refreshComplete');
  };
  $scope.judul=function (judul) {
    judul_kategori.set(judul);
  }
  $scope.getjudul=function () {
    var tmp_judul=judul_kategori.get();
    return tmp_judul['name'];
  }
  $scope.getslug=function () {
    var tmp_judul=judul_kategori.get();
    return tmp_judul['slug'];
  }
  $scope.news_category = function(){
    $http.get(
      'http://www.jfxjournal.com/api/posts/by_category/'+$stateParams.cat+''
    ).success(function(data){
      $scope.result_news_category=data.result;
      $ionicLoading.show({
           template: 'Selesai memuat berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
            $ionicSlideBoxDelegate.update();
        }, 1000);
    }).error(function(){
      $ionicLoading.show({
           template: 'Gagal memuat berita'
        });
      $timeout(function () {
            $ionicLoading.hide();
        }, 5000);
    });

  };
});