// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    $cordovaPlugin.someFunction().then(success, error);
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    cache: false,
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.beranda', {
      url: '/beranda',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/beranda.html',
          controller: 'AppCtrl'
        }
      }
    })

  .state('app.beranda_berita_terbaru', {
      url: '/beranda_berita_terbaru/:id',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/beranda_berita_terbaru.html',
          controller: 'AppCtrl'
        }
      }
    })

  .state('app.beranda_berita_populer', {
      url: '/beranda_berita_populer/:id',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/beranda_berita_populer.html',
          controller: 'AppCtrl'
        }
      }
    })

  .state('app.category', {
      url: '/category/:cat',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/category.html',
          controller: 'AppCtrl'
        }
      }
  })
  .state('app.category_detail', {
      url: '/category_detail/:id',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/category_detail.html',
          controller: 'AppCtrl'
        }
      }
  })


  // state('.app.us_market',{
  //     url: '/us_market',
  //     views: {
  //       'menuContent': {
  //         templateUrl: 'templates/us_market.html',
  //         controller: 'AppCtrl'
  //       }
  //     }
  // })
  // state('.app.european_market',{
  //     url: '/european_market',
  //     views: {
  //       'menuContent': {
  //         templateUrl: 'templates/european_market.html',
  //         controller: 'AppCtrl'
  //       }
  //     }
  // })
  // state('.app.asian_market',{
  //     url: '/asian_market',
  //     views: {
  //       'menuContent': {
  //         templateUrl: 'templates/asian_market.html',
  //         controller: 'AppCtrl'
  //       }
  //     }
  // })
  // state('.app.forex',{
  //     url: '/forex',
  //     views: {
  //       'menuContent': {
  //         templateUrl: 'templates/forex.html',
  //         controller: 'AppCtrl'
  //       }
  //     }
  // })
  // state('.app.saham',{
  //     url: '/saham',
  //     views: {
  //       'menuContent': {
  //         templateUrl: 'templates/saham.html',
  //         controller: 'AppCtrl'
  //       }
  //     }
  // })
  // state('.app.komoditi',{
  //     url: '/komoditi',
  //     views: {
  //       'menuContent': {
  //         templateUrl: 'templates/komoditi.html',
  //         controller: 'AppCtrl'
  //       }
  //     }
  // })
  // state('.app.signal_trading',{
  //     url: '/signal_trading',
  //     views: {
  //       'menuContent': {
  //         templateUrl: 'templates/signal_trading.html',
  //         controller: 'AppCtrl'
  //       }
  //     }
  // })
  // state('.app.lifestyle',{
  //     url: '/lifestyle',
  //     views: {
  //       'menuContent': {
  //         templateUrl: 'templates/lifestyle.html',
  //         controller: 'AppCtrl'
  //       }
  //     }
  // })
  // state('.app.infografis',{
  //     url: '/infografis',
  //     views: {
  //       'menuContent': {
  //         templateUrl: 'templates/infografis.html',
  //         controller: 'AppCtrl'
  //       }
  //     }
  // })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/beranda');
});